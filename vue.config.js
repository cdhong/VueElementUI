const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  // 配置跨域请求
  // 解决 WebSocket connection to 'ws://192.168.10.103:8080/ws' failed 
  devServer: {
    // 项目运行的端口号配置
    // port: 3000,
    // 自动打开浏览器
    // open: true,
    // https: false,
    // proxy: {
    //   "/api": {
    //     // 要请求的后端接口
    //     target: "",
    //     ws: true, // 用于支持websocket，不写默认为true
    //     // 开启跨域
    //     changeOrigin: true,
    //     // secure: true, // 如果是 https,需要开启这个选项
    //     // 替换 target 中的请求地址，也就是说，以后请求地址时候，直接写成/api即可
    //     pathRewrite: {
    //       "^/api": "",
    //     },
    //   },
    // },
    // 添加后， WebSocket 就不会报错了
  },
});
