import Mock from "mockjs";

let List = [];

export default {
  getStatistionData: () => {
    for (let i = 0; i < 7; i++) {
      List.push(
        Mock.mock({
          Html5: Mock.Random.float(100, 8001, 0, 0),
          CSS3: Mock.Random.float(101, 8002, 0, 0),
          JavaScript: Mock.Random.float(102, 8003, 0, 0),
          Nodejs: Mock.Random.float(103, 8004, 0, 0),
          Vue2: Mock.Random.float(104, 8005, 0, 0),
          Vue3: Mock.Random.float(105, 8006, 0, 0),
        })
      );
    }
    return {
      code: 20000,
      data: {
        // 饼图
        videoData: [
          { name: "华为", value: 2999 },
          { name: "小米", value: 2000 },
          { name: "oppo", value: 1000 },
          { name: "vivo", value: 1500 },
          { name: "苹果", value: 6000 },
        ],
        // 柱状图
        userData: [
          { data: "周一", new: 5, action: 200 },
          { data: "周二", new: 10, action: 300 },
          { data: "周三", new: 12, action: 400 },
          { data: "周四", new: 50, action: 600 },
          { data: "周五", new: 80, action: 500 },
          { data: "周六", new: 50, action: 700 },
          { data: "周日", new: 30, action: 800 },
        ],
        // 折线图
        gradData: {
          date: [
            "20190101",
            "20190102",
            "20190103",
            "20190104",
            "20190105",
            "20190106",
            "20190107",
          ],
          data: List,
        },
        tableData: [
          {
            curriculum: "Html5",
            id: "1",
            today: "500",
            thismonth: "1000",
            all: "1500",
          },
          {
            curriculum: "CSS3",
            id: "2",
            today: "999",
            thismonth: "201",
            all: "1200",
          },
          {
            curriculum: "JavaScript",
            id: "3",
            today: "199",
            thismonth: "2001",
            all: "2200",
          },
          {
            curriculum: "Nodejs",
            id: "4",
            today: "299",
            thismonth: "1201",
            all: "1500",
          },
          {
            curriculum: "Vue2",
            id: "5",
            today: "1201",
            thismonth: "2799",
            all: "4000",
          },
          {
            curriculum: "Vue3",
            id: "1",
            today: "1000",
            thismonth: "500",
            all: "1500",
          },
          {
            curriculum: "Html5",
            id: "1",
            today: "1000",
            thismonth: "500",
            all: "1500",
          },
        ],
        countData: [
          {
            name: "今日支付订单",
            value: 1233,
            icon: "success",
            color: "#8fbc8f",
          },
          {
            name: "本月购买",
            value: 20000,
            icon: "success",
            color: "#527f76",
          },
          {
            name: "总购买",
            value: 39984,
            icon: "success",
            color: "skyblue",
          },
          {
            name: "Html5",
            value: 500,
            icon: "success",
            color: "#e9c2a6",
          },
          {
            name: "CSS3",
            value: 999,
            icon: "success",
            color: "#3299cc",
          },
          {
            name: "JavaScript",
            value: 199,
            icon: "success",
            color: "#9f5f9f",
          },
        ],
      },
    };
  },
};
