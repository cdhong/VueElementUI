import axios from 'axios'

const serve = axios.create({
    baseURL:'/api',
    timeout:5000
})

// 请求拦截器
serve.interceptors.request.use(config =>{
    // 对请求数据做些什么
    // if (window.localStorage.getItem("token")) {
    //     config.headers.common['Access-Token'] = window.localStorage.getItem("token");
    // }
    return config
}, function (error){
    // 对请求错误做些什么
    return Promise.reject(error)
})
// 响应拦截器
serve.interceptors.response.use(res=>{
    // 对响应数据做些什么
    return res
},function(res){
    // 对响应错误做些什么
    return Promise.reject(res)
})


export default serve