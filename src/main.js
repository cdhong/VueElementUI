import Vue from "vue";
import App from "./App.vue";
import "./common/css/base.less";
// 引入路由器
import router from "./router";
// 引入Vuex
import store from "./store";
// 引入element-ui 样式 组件
import "element-ui/lib/theme-chalk/index.css";
import element from "./element/assembly";
// mock  前端用来模拟后端请求数据
import "./utils/mock/mock";

// 控制提示信息的开关
Vue.config.productionTip = false;
// 全局挂载
Vue.use(element);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
