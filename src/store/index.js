import Vue from 'vue'
import Vuex from 'vuex'
import persistedState from 'vuex-persistedstate'
import tab from './tab'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        tab
    },
    plugins: [persistedState()]
})




