export default {
  state: {
    isCollapse: false, // 菜单展开收起
    tabList: [
      {
        id: "1",
        label: "首页",
        name: "index",
        navicon: "house",
        path: "/index",
      },
    ], // 面包屑
  },
  mutations: {
    collapseMenu(state) {
      state.isCollapse = !state.isCollapse;
    },
    selectMenu(state, val) {
      // 判断数据是否为首页
      if (val.name !== "index") {
        const index = state.tabList.findIndex((item) => item.name === val.name);
        console.log(1, index);
        // 如果不存在
        if (index === -1) {
          state.tabList.push(val);
        }
      }
    },
    // 删除指定tag数据
    closeTag(state, item) {
      const index = state.tabList.findIndex((val) => val.name === item.name);
      state.tabList.splice(index, 1);
    },
    // 删除除首页要的其他tag
    closeTagAll(state) {
      let data = state.tabList[0];
      state.tabList = [];
      state.tabList.push(data);
    },
  },
};
